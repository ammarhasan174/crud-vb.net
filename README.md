

# CRUD application using VB.NET

  

Hello,

  

this is a simple example of a CRUD app for managing Items.

  

  

## How To Run

  

- Import the database **CRUD.bacpac** to your Database server.

  

- Build the project by doing these two steps:
		**1** Build -> Build Solution
		**2** Build -> Build WebApplication1 
  

- Get the connection string and add it in **Web.config** File in the tag called **CRUDConnectionString**

  

- and you are all set and ready to run the project

  

  

## Use Cases

  

### Read:

  

- Once the Home Page loads it will automatically load all the data in the Database and forward them the GridView

  

### Insert:

  

- To insert new item the the database click in add item and a new page will load.

  

- Add your product details and hit **save**

  

- Now it's added the database and will show up on the GridView in Home Page

  

### Update:

  

- To update the item click on Edit button and a new page will load

  

- This page contains the item details

  

- Edit them as you like and hit **save**

  

- now the updated item will show in the GridView in Home Page

  

### Delete

  

- In order to delete an item from the database click on **Edit** button and a new page will load

  

- click on **Delete** button and it will be showing you a confirmation dialog

  

- click ok in order to confirm the delete process

- the item will be deleted and removed from the GridView in the Home Page
  # Example Video
    ![17-05-21](/uploads/51e038cfc862bcf96bcf56b6c9025bdc/17-05-21.mp4)


