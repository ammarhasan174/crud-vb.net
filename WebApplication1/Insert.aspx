﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Insert.aspx.vb" Inherits="WebApplication1.Insert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insert Data</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
        }
        .form-table {
            width: 100%;
        }
        .form-table td {
            padding: 10px 0;
        }
        .form-table label {
            font-weight: bold;
            width: 150px;
            display: inline-block;
        }
        .form-table input[type="text"],
        .form-table input[type="number"],
        .form-table input[type="date"],
        .form-table input[type="radio"] {
            width: calc(100% - 160px);
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        #btnSave {
            background-color: #007bff;
            color: #fff;
            border: none;
            padding: 10px;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            width: 100%;
        }
        .color-radio label {
    display: inline-block;
    cursor: pointer;
    margin-right: 10px;
}

.color-radio input[type="radio"] {
    display: none;
}

.color-radio input[type="radio"] + label {
    padding: 5px 10px;
    border: 1px solid #ccc;
    border-radius: 4px;
}

.color-radio input[type="radio"]:checked + label {
    background-color: #007bff;
    color: #fff;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <table class="form-table">
                <tr>
                    <td>
                        <label for="txtName">Name:</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtSpecs">Specifications:</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSpecs" runat="server" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="radioListColors">Color:</label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="radioListColors" runat="server" CellPadding="0" CellSpacing="0" CssClass="color-radio" RepeatDirection="Horizontal" Width="100%">
                            <asp:ListItem>RED</asp:ListItem>
                            <asp:ListItem>BLUE</asp:ListItem>
                            <asp:ListItem>GREEN</asp:ListItem>
                            <asp:ListItem>BLACK</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtQnt">Quantity:</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtQnt" runat="server" Width="100%" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtWeight">Weight:</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtWeight" runat="server" Width="100%" TextMode="Number"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtExpDate">ExpDate:</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtExpDate" runat="server" type="date" Width="100%"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
        </div>
    </form>
</body>
</html>
