﻿Public Class Item
    Public Property Id As Integer
    Public Property ItemName As String
    Public Property Specs As String
    Public Property Color As String
    Public Property Quantity As Integer
    Public Property Weight As Double
    Public Property ExpDate As Date
    Public Property CreateAt As Date
End Class