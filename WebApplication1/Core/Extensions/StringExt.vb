﻿Imports System.Runtime.CompilerServices

Module StringExtensions
    <Extension()>
    Public Function RemoveWhiteSpace(inputString As String) As String
        Dim pattern As String = "\s"
        Dim result As String = Regex.Replace(inputString, pattern, String.Empty)
        Return result
    End Function
End Module
