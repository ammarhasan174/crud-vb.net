﻿Imports System.Data.Common
Imports System.Data.SqlClient

Public Class MyHome
    Inherits System.Web.UI.Page
    Private dbHelper As New DbHelperImpl()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ListProduct()
    End Sub

    Protected Sub GridViewMyHome_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridViewMyHome.SelectedIndexChanged



    End Sub

    Public Sub ListProduct()
        Dim dt As DataTable = dbHelper.GetDataTable()

        GridViewMyHome.DataSource = dt
        GridViewMyHome.DataBind()
    End Sub

    Private Sub GridViewMyHome_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles GridViewMyHome.RowEditing
        Dim index As Integer = e.NewEditIndex
        Dim editedRow As GridViewRow = GridViewMyHome.Rows(index)
        Dim Id As Integer = Convert.ToInt32(editedRow.Cells(1).Text)
        Response.Redirect("Edit.aspx?Id=" & Id)

    End Sub

    Protected Sub btnInsertNew_Click(sender As Object, e As EventArgs) Handles btnInsertNew.Click
        Response.Redirect("Insert.aspx")
    End Sub

End Class