﻿Imports System.Data.SqlClient

Public Class DbHelperImpl
    Inherits DBHelper
    Private connect As New SqlConnection(ConfigurationManager.ConnectionStrings("CRUDConnectionString").ConnectionString)

    Public Overrides Function Add(item As Item)
        connect.Open()
        Dim command As New SqlCommand("
        INSERT INTO Items 
        (ItemName,
        Specs,
        Color,
        Quantity,
        Weight,
        ExpDate)        
        Values (
        '" & item.ItemName & "',
        '" & item.Specs & "',
        '" & item.Color & "',
        '" & item.Quantity & "',
        '" & item.Weight & "',
        '" & item.ExpDate & "')
         ", connect)

        command.ExecuteNonQuery()
        connect.Close()
    End Function

    Public Overrides Function Retrive(Id As Integer) As Item

        Dim command As New SqlCommand()
        Dim reader As SqlDataReader
        Dim stmt As String
        Dim item As New Item()
        connect.Open()

        stmt = "SELECT * FROM Items Where id = '" & Id & "';"
        command = New SqlCommand(stmt, connect)
        reader = command.ExecuteReader()
        reader.Read()
        item.Id = reader.GetInt32(0)
        item.ItemName = reader.GetString(1)
        item.Specs = reader.GetString(2)
        item.Color = reader.GetString(3)
        item.Quantity = reader.GetInt32(4)
        item.Weight = Convert.ToDecimal(reader.GetValue(5).ToString())
        item.ExpDate = reader.GetDateTime(6)
        item.CreateAt = reader.GetDateTime(7)
        connect.Close()
        Return item
    End Function

    Public Overrides Function Delete(Id As Integer)
        connect.Open()
        Dim command As New SqlCommand("
        DELETE FROM Items WHERE id = '" & Id & "'
         ", connect)
        command.ExecuteNonQuery()
        connect.Close()

    End Function

    Public Overrides Function Update(item As Item)
        connect.Open()
        Dim command As New SqlCommand("Update Items SET 
        ItemName = '" & item.ItemName & "',
        Specs = '" & item.Specs & "',
        Color = '" & item.Color & "',
        Quantity = '" & item.Quantity & "',
        Weight = '" & item.Weight & "',
        ExpDate = '" & item.ExpDate & "'
         WHERE Id = '" & item.Id & "'", connect)

        command.ExecuteNonQuery()
        connect.Close()

    End Function

    Public Overrides Function GetList() As List(Of Item)
        Dim items As New List(Of Item)()
        Dim command As New SqlCommand()
        Dim reader As SqlDataReader
        Dim stmt As String
        connect.Open()
        stmt = "SELECT * FROM Items"
        command = New SqlCommand(stmt, connect)
        reader = command.ExecuteReader()
        While reader.Read()
            Dim item As New Item()
            item.Id = reader.GetInt32(0)
            item.ItemName = reader.GetString(1)
            item.Specs = reader.GetString(2)
            item.Color = reader.GetString(3)
            item.Quantity = reader.GetInt32(4)
            item.Weight = Convert.ToDecimal(reader.GetValue(5).ToString())
            item.ExpDate = reader.GetDateTime(6)
            item.CreateAt = reader.GetDateTime(7)
            items.Add(item)
        End While
        connect.Close()

        Return items
    End Function
    Public Overrides Function GetDataTable() As DataTable
        Dim command As New SqlCommand("select * from Items", connect)
        Dim sd As New SqlDataAdapter(command)
        Dim dt As New DataTable
        sd.Fill(dt)
        Return dt
    End Function
End Class
