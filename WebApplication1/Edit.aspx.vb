﻿Imports System.Net.NetworkInformation
Imports Microsoft.Ajax.Utilities

Public Class Edit
    Inherits System.Web.UI.Page
    Private db_helper As New DbHelperImpl()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim itemId As String = Request.QueryString("Id")
            Dim item As New Item()
            item = db_helper.Retrive(itemId)
            txtName.Text = item.ItemName
            txtSpecs.Text = item.Specs
            radioListColors.SelectedValue = item.Color.RemoveWhiteSpace()
            txtQnt.Text = item.Quantity
            txtWeight.Text = item.Weight
            txtExpDate.Text = item.ExpDate.ToString("yyyy-MM-dd")
        End If
    End Sub

    Protected Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged

    End Sub



    Protected Sub txtName0_TextChanged(sender As Object, e As EventArgs) Handles txtExpDate.TextChanged

    End Sub

    Protected Sub txtQnt_TextChanged(sender As Object, e As EventArgs) Handles txtQnt.TextChanged

    End Sub

    Protected Sub radioListColors_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radioListColors.SelectedIndexChanged

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim itemId As Integer = Convert.ToInt32(Request.QueryString("Id"))

        db_helper.Delete(itemId)
        Response.Redirect("MyHome.aspx")
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim itemId As Integer = Convert.ToInt32(Request.QueryString("Id"))

        Dim Name As String = txtName.Text
        Dim Specs As String = txtSpecs.Text
        Dim Color As String = radioListColors.SelectedValue
        Dim Quantity As Integer = 0
        Integer.TryParse(txtQnt.Text.ToString(), Quantity)
        Dim Weight As Decimal = 0.0
        Decimal.TryParse(txtWeight.Text.ToString(), Weight)
        Dim ExpDate As DateTime = DateTime.Parse(txtExpDate.Text.ToString())

        Dim testA As Boolean = String.IsNullOrEmpty(Name.RemoveWhiteSpace()) = True
        Dim testB As Boolean = String.IsNullOrEmpty(Specs.RemoveWhiteSpace()) = True
        Dim testC As Boolean = String.IsNullOrEmpty(Color) = True

        Dim InValid As Boolean = String.IsNullOrEmpty(Name.RemoveWhiteSpace()) = True _
            OrElse String.IsNullOrEmpty(Specs.RemoveWhiteSpace()) = True _
            OrElse String.IsNullOrEmpty(Color) = True
        If InValid Then
            MsgBox("Enter valid data please.")
            Return
        End If

        Dim item As New Item()
        item.Id = itemId
        item.ItemName = Name
        item.Specs = Specs
        item.Color = Color
        item.Quantity = Quantity
        item.Weight = Weight
        item.ExpDate = ExpDate
        db_helper.Update(item)
        Response.Redirect("MyHome.aspx")
    End Sub
End Class