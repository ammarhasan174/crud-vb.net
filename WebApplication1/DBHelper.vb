﻿Public MustInherit Class DBHelper
    Public MustOverride Function Add(ByVal item As Item)
    Public MustOverride Function Retrive(ByVal Id As Integer) As Item
    Public MustOverride Function Delete(ByVal Id As Integer)
    Public MustOverride Function Update(ByVal item As Item)
    Public MustOverride Function GetList() As List(Of Item)
    Public MustOverride Function GetDataTable() As DataTable
End Class
