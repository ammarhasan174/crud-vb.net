﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MyHome.aspx.vb" Inherits="WebApplication1.MyHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CRUD Example</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f5f5f5;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            animation: slide-in 0.5s ease-out;
        }
        .header {
            background-color: #007bff;
            color: #fff;
            text-align: center;
            padding: 10px;
            margin-bottom: 20px;
            border-top-left-radius: 8px;
            border-top-right-radius: 8px;
        }
        .gridview-container {
            text-align: center;
            margin-bottom: 20px;
            animation: fade-in 0.5s ease-out;
        }
        #btnInsertNew {
            background-color: #28a745;
            color: #fff;
            border: none;
            padding: 10px 20px;
            font-size: 16px;
            cursor: pointer;
            border-radius: 5px;
            transition: background-color 0.3s ease-out;
        }
        #btnInsertNew:hover {
            background-color: #218838;
        }

        
        @keyframes slide-in {
            from {
                transform: translateY(-20%);
                opacity: 0;
            }
            to {
                transform: translateY(0);
                opacity: 1;
            }
        }

        @keyframes fade-in {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="header"> 
            <h2>CRUD Example using VB.NET</h2>
        </div>
        <div class="container">
            <div class="gridview-container">
                <asp:GridView ID="GridViewMyHome" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="myGrid" EmptyDataText="Add item below">
                    <AlternatingRowStyle BackColor="#f2f2f2" />
                    <Columns>
                        <asp:ButtonField CommandName="Edit" Text="Edit" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <EmptyDataRowStyle Font-Size="Medium" />
                    <FooterStyle BackColor="#007bff" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#007bff" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#007bff" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#ffffff" />
                    <SelectedRowStyle BackColor="#d1d1d1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#ffffff" />
                    <SortedAscendingHeaderStyle BackColor="#a6a6a6" />
                    <SortedDescendingCellStyle BackColor="#c8c8c8" />
                    <SortedDescendingHeaderStyle BackColor="#848484" />
                </asp:GridView>
            </div>
            <div style="text-align: center;">
                <asp:Button ID="btnInsertNew" runat="server" Text="Add Item" OnClick="btnInsertNew_Click" />
            </div>
        </div>
    </form>

    
</body>
</html>
