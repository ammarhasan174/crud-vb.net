﻿Public Class Insert
    Inherits System.Web.UI.Page
    Private db_helper As New DbHelperImpl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtExpDate.Text = DateTime.Today.ToString("yyyy-MM-dd")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
            Dim Name As String = txtName.Text
            Dim Specs As String = txtSpecs.Text
            Dim Color As String = radioListColors.SelectedValue
            Dim Quantity As Integer = 0
        Integer.TryParse(txtQnt.Text.RemoveWhiteSpace().ToString(), Quantity)
        Dim Weight As Decimal = 0.0
        Decimal.TryParse(txtWeight.Text.RemoveWhiteSpace().ToString(), Weight)
        Dim ExpDate As DateTime = Nothing
        DateTime.TryParse(txtExpDate.Text.ToString(), ExpDate)


        Dim InValid As Boolean = String.IsNullOrEmpty(Name.RemoveWhiteSpace()) = True _
            OrElse String.IsNullOrEmpty(Specs.RemoveWhiteSpace()) = True _
            OrElse ExpDate = Nothing _
            OrElse String.IsNullOrEmpty(Color) = True
        If InValid Then
                MsgBox("Enter valid data please.")
                Return
            End If
        Dim item As New Item()
        item.ItemName = Name
        item.Specs = Specs
            item.Color = Color
            item.Quantity = Quantity
            item.Weight = Weight
            item.ExpDate = ExpDate
        db_helper.Add(item)
        Response.Redirect("MyHome.aspx")
        End Sub

    Protected Sub txtExpDate_TextChanged(sender As Object, e As EventArgs) Handles txtExpDate.TextChanged

    End Sub
End Class